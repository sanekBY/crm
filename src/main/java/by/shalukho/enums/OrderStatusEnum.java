package by.shalukho.enums;

public enum OrderStatusEnum {
    OPENED, IN_PROGRESS, CLOSED
}
